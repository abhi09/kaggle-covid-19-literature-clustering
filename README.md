# COVID-19 Open Research Dataset Challenge (CORD-19).

https://www.kaggle.com/allen-institute-for-ai/CORD-19-research-challenge

In response to the COVID-19 pandemic, the White House and a coalition of leading research groups have prepared the COVID-19 Open Research Dataset (CORD-19). CORD-19 is a resource of over 195,000 scholarly articles, including over 87,000 with full text, about COVID-19, SARS-CoV-2, and related coronaviruses. This freely available dataset is provided to the global research community to apply recent advances in natural language processing and other AI techniques to generate new insights in support of the ongoing fight against this infectious disease. There is a growing urgency for these approaches because of the rapid acceleration in new coronavirus literature, making it difficult for the medical research community to keep up.

# APPROACH

Major Steps in this project are
1. Convert unsupervised data to supervised data
    * Parse the text from the body of each document using Natural Language Processing (NLP).
    * Turn each document instance  𝑑𝑖  into a feature vector  𝑋𝑖  using Term Frequency–inverse Document Frequency (TF-IDF).
    * Apply Dimensionality Reduction to each feature vector  𝑋𝑖  using t-Distributed Stochastic Neighbor Embedding (t-SNE) to cluster similar research articles in the two dimensional plane  𝑋  embedding  𝑌1 .
    * Use Principal Component Analysis (PCA) to project down the dimensions of  𝑋  to a number of dimensions that will keep .95 variance while removing noise and outliers in embedding  𝑌2 .
    * Apply k-means clustering on  𝑌2 , where  𝑘  is 20, to label each cluster on  𝑌1 .
2. Apply Topic Modeling on  𝑋  using Latent Dirichlet Allocation (LDA) to discover keywords from each cluster.
3. Investigate the clusters visually on the plot, zooming down to specific articles as needed, and via classification using Stochastic Gradient Descent (SGD).